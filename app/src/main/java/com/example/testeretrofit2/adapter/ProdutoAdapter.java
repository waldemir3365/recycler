package com.example.testeretrofit2.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.testeretrofit2.R;
import com.example.testeretrofit2.interfaces.OnListClickListener;
import com.example.testeretrofit2.models.Data;
import com.example.testeretrofit2.viewHolder.ProdutoViewHolder;

import java.util.List;

public class ProdutoAdapter  extends RecyclerView.Adapter {

    private List<Data> listData;
    private Context context;
    private OnListClickListener listener;


    public ProdutoAdapter(List<Data> listData, Context context, OnListClickListener listener) {

        this.listData = listData;
        this.context  = context;
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {


        View view = LayoutInflater.from(context).inflate(R.layout.row_prod_list, viewGroup, false);

        return new ProdutoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        ProdutoViewHolder holder = (ProdutoViewHolder) viewHolder;
        Data data = listData.get(position);
        holder.setFiels(data, listener);

    }

    @Override
    public int getItemCount() {
        return listData.size();
    }
}
