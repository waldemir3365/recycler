package com.example.testeretrofit2.interfaces;

import com.example.testeretrofit2.models.Data;

public interface OnListClickListener {

    void onClick(Data data);
}
