package com.example.testeretrofit2.interfaces;

import com.example.testeretrofit2.models.ListData;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ProdutoService {


    @GET("produto/maisvendidos")
    Call<ListData>getProducts();
}
