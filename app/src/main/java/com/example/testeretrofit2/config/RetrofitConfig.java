package com.example.testeretrofit2.config;

import com.example.testeretrofit2.interfaces.ProdutoService;

import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class RetrofitConfig {

    private final Retrofit retrofit;


    public RetrofitConfig() {

        this.retrofit = new Retrofit.Builder()
                            .baseUrl("https://alodjinha.herokuapp.com/")
                            .addConverterFactory(JacksonConverterFactory.create())
                            .build();
    }


    public ProdutoService getProdutoService(){

        return this.retrofit.create(ProdutoService.class);
    }




}
