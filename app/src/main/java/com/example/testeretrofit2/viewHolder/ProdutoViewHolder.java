package com.example.testeretrofit2.viewHolder;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.testeretrofit2.R;
import com.example.testeretrofit2.interfaces.OnListClickListener;
import com.example.testeretrofit2.models.Data;
import com.squareup.picasso.Picasso;

public class ProdutoViewHolder extends RecyclerView.ViewHolder {


    private ImageView img_foto;
    private TextView text_nome;
    private TextView text_precoDe;
    private TextView text_precoPor;
    private CardView cardView;
    private Context context;


    public ProdutoViewHolder(@NonNull View itemView) {
        super(itemView);

        text_nome = itemView.findViewById(R.id.text_nome);
        img_foto = itemView.findViewById(R.id.img_foto);
        text_precoDe = itemView.findViewById(R.id.text_precoDe);
        text_precoPor = itemView.findViewById(R.id.text_precoPor);
        cardView = itemView.findViewById(R.id.card_view);
        context = itemView.getContext();
    }

    public void setFiels(Data data, OnListClickListener listener) {

        this.text_nome.setText(data.getNome());
        this.text_precoPor.setText(String.valueOf(data.getPrecoPor()));
        this.text_precoDe.setText(String.valueOf(data.getPrecoDe()));
        loadImage(img_foto, data.getUrlImagem());
        this.cardView.setOnClickListener(clickTexto(listener, data));
    }

    private void loadImage(ImageView img_foto,String url) {

        Picasso.with(context)
                .load(url)
                .error(R.drawable.ic_launcher_background)
                .into(img_foto);
    }

    private View.OnClickListener clickTexto(OnListClickListener listener, Data data) {
        return view->{
            listener.onClick(data);
        };
    }
}
