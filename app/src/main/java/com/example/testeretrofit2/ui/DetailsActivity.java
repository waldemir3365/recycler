package com.example.testeretrofit2.ui;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.testeretrofit2.R;
import com.example.testeretrofit2.models.Data;
import com.example.testeretrofit2.utils.ToastUtil;
import com.squareup.picasso.Picasso;

public class DetailsActivity extends AppCompatActivity {

    private TextView nome;
    private TextView valor;
    private TextView descricao;
    private ImageView image;
   private  FloatingActionButton fab;
   private Data data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        nome = findViewById(R.id.text_nome);
        valor = findViewById(R.id.text_valor);
        descricao = findViewById(R.id.text_description);
        image = findViewById(R.id.img_foto);
        fab = findViewById(R.id.fab);

        init();

    }

    private void init(){

        recoveryData();
        setFields();
        fab.setOnClickListener(clickFab());
    }

    private View.OnClickListener clickFab() {

        return view->{
            ToastUtil.ToastLong("click me", getBaseContext());
        };
    }

    private void setFields() {

        nome.setText(data.getNome());
        valor.setText(String.valueOf(data.getPrecoPor()));
        descricao.setText(validateDescription());
        setImage(image);
    }

    private String validateDescription() {
        return data.getDescricao().replace("<br/>", "");
    }

    private void setImage(ImageView image) {

        Picasso.with(getBaseContext())
                .load(data.getUrlImagem())
                .error(R.drawable.ic_launcher_background)
                .into(image);
    }

    private void recoveryData() {
        data = (Data) getIntent().getSerializableExtra(data.DATA);
    }
}
