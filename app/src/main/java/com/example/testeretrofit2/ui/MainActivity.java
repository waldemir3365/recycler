package com.example.testeretrofit2.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.testeretrofit2.R;
import com.example.testeretrofit2.config.RetrofitConfig;
import com.example.testeretrofit2.adapter.ProdutoAdapter;
import com.example.testeretrofit2.interfaces.OnListClickListener;
import com.example.testeretrofit2.models.Data;
import com.example.testeretrofit2.models.ListData;
import com.example.testeretrofit2.utils.NavigateUtils;
import com.example.testeretrofit2.utils.ToastUtil;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {


    ViewHolder mViewHolder = new ViewHolder();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.mViewHolder.recyclerView = findViewById(R.id.recycler);
        init();
    }

    private void init(){

        getCall();
    }

    private void getCall(){

        Call<ListData> call  = new RetrofitConfig().getProdutoService().getProducts();
        call.enqueue(getService());
    }

    private Callback<ListData> getService() {

        return new Callback<ListData>() {
            @Override
            public void onResponse(Call<ListData> call, Response<ListData> response) {

              ListData data  = response.body();

              setAdapterList(data.getData());

            }

            @Override
            public void onFailure(Call<ListData> call, Throwable t) {

                ToastUtil.ToastLong("deu merda " + t.getMessage(), getBaseContext());

            }
        };
    }

    //definir um adapter para recycler view
    private void setAdapterList(List<Data> data) {

        mViewHolder.recyclerView.setAdapter(new ProdutoAdapter(data,getBaseContext(),clickOnclickLister()));
        mViewHolder.recyclerView.addItemDecoration(new DividerItemDecoration(getBaseContext(),DividerItemDecoration.VERTICAL));
        setLayoutManager();
    }

    private OnListClickListener clickOnclickLister() {

        return new OnListClickListener() {
            @Override
            public void onClick(Data data) {

                NavigateUtils.Navigate(getBaseContext(),DetailsActivity.class,data);
            }
        };
    }

    // definir um layout
    private void setLayoutManager(){

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL, false);
        this.mViewHolder.recyclerView.setLayoutManager(layoutManager);
    }

    private static class ViewHolder{

        RecyclerView recyclerView;
    }

}
