package com.example.testeretrofit2.utils;

import android.content.Context;
import android.content.Intent;

import com.example.testeretrofit2.models.Data;

public class NavigateUtils {

    private static Intent i;

    public static void Navigate(Context context, Class clazz, Data d){

        i = new Intent(context, clazz);
        i.putExtra(d.DATA,d);
        context.startActivity(i);

    }
}
