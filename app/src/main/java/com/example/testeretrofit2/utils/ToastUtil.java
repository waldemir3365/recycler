package com.example.testeretrofit2.utils;

import android.content.Context;
import android.widget.Toast;

public class ToastUtil {


    public static void ToastLong(String mensagem, Context context){

        Toast.makeText(context, mensagem, Toast.LENGTH_LONG).show();
    }


    public static void ToastShort(String msg, Context context){

        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }
}
